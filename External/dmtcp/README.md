Distributed MultiThreaded CheckPointing
========================================

This package builds the dmtcp library for the offline software of ATLAS.

The library's sources are taken from https://github.com/dmtcp/dmtcp
