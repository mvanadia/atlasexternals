# Copyright (C) 2002-2017 CERN for the benefit of the ATLAS collaboration
#
# Package building CLHEP as part of the offline build procedure.
#

# The name of the package:
atlas_subdir( CLHEP )

# In release recompilation mode stop here:
if( ATLAS_RELEASE_MODE )
   return()
endif()

# The source to build:
set( _source
   http://proj-clhep.web.cern.ch/proj-clhep/DISTRIBUTION/tarFiles/clhep-2.2.0.4.tgz )
set( _md5 71d2c7c2e39d86a0262e555148de01c1 )

# Extra options for the configuration:
set( _extraOptions )
if( "${CMAKE_CXX_STANDARD}" GREATER 11 OR
      "${CMAKE_CXX_STANDARD}" EQUAL 11 )
   list( APPEND _extraOptions -DCMAKE_CXX_STANDARD=${CMAKE_CXX_STANDARD}
      -DCLHEP_BUILD_CXXSTD=ON )
endif()

if( NOT "${CMAKE_BUILD_TYPE}" STREQUAL "" )
   list( APPEND _extraOptions -DCMAKE_BUILD_TYPE=${CMAKE_BUILD_TYPE} )
endif()

# Get the number of cores in the build machine. Building CLHEP using all the
# cores overcommits the build machine a bit, but that should be okay.
atlas_cpu_cores( nCPUs )

# Directory for the temporary build results:
set( _buildDir ${CMAKE_CURRENT_BINARY_DIR}${CMAKE_FILES_DIRECTORY}/CLHEPBuild )

# Build CLHEP in the build area:
ExternalProject_Add( CLHEP
   PREFIX ${CMAKE_BINARY_DIR}
   INSTALL_DIR ${CMAKE_BINARY_DIR}/${ATLAS_PLATFORM}
   URL ${_source}
   URL_MD5 ${_md5}
   PATCH_COMMAND ${CMAKE_COMMAND} -E copy_directory
   <SOURCE_DIR>/CLHEP/ <SOURCE_DIR>
   COMMAND ${CMAKE_COMMAND} -E remove_directory <SOURCE_DIR>/CLHEP
   CMAKE_CACHE_ARGS
   -DCMAKE_INSTALL_PREFIX:PATH=${_buildDir}
   -DCLHEP_BUILD_DOCS:BOOL=OFF
   CMAKE_GENERATOR "Unix Makefiles"
   CMAKE_ARGS
   ${_extraOptions}
   LOG_CONFIGURE 1
   BUILD_COMMAND make -j${nCPUs} )
ExternalProject_Add_Step( CLHEP buildinstall
   COMMAND ${CMAKE_COMMAND} -E remove_directory
   ${_buildDir}/lib/CLHEP-2.2.0.4
   COMMAND ${CMAKE_COMMAND} -E copy_directory
   ${_buildDir}/ <INSTALL_DIR>
   COMMENT "Installing CLHEP into the build area"
   DEPENDEES install )
add_dependencies( Package_CLHEP CLHEP )

# Install CLHEP:
install( DIRECTORY ${_buildDir}/
   DESTINATION . USE_SOURCE_PERMISSIONS OPTIONAL )
