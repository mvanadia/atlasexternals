# Copyright (C) 2002-2017 CERN for the benefit of the ATLAS collaboration
#
# Package building Eigen as part of the offline / analysis software build.
#

# The name of the package:
atlas_subdir( Eigen )

# In release recompilation mode stop here:
if( ATLAS_RELEASE_MODE )
   return()
endif()

# Decide whether to build Eigen:
find_package( Eigen QUIET )
set( _flag TRUE )
if( EIGEN_FOUND )
   set( _flag FALSE )
endif()
option( ATLAS_BUILD_EIGEN "Build Eigen as part of the release" ${_flag} )
unset( _flag )

# Stop if the build is not needed:
if( NOT ATLAS_BUILD_EIGEN )
   return()
endif()

# Tell the user what's happening:
message( STATUS "Building Eigen as part of this project" )

# The source of eigen:
set( _eigenSource
   "http://cern.ch/lcgpackages/tarFiles/sources/eigen-3.2.9.tar.gz" )
set( _eigenMd5 "de04f424e6b86907ccc9737b5d3048e7" )

# Temporary directory for the build results:
set( _buildDir ${CMAKE_CURRENT_BINARY_DIR}${CMAKE_FILES_DIRECTORY}/EigenBuild )

# Build Eigen for the build area:
ExternalProject_Add( Eigen
   PREFIX ${CMAKE_BINARY_DIR}
   INSTALL_DIR ${CMAKE_BINARY_DIR}/${ATLAS_PLATFORM}
   URL ${_eigenSource}
   URL_MD5 ${_eigenMd5}
   CMAKE_CACHE_ARGS -DCMAKE_INSTALL_PREFIX:PATH=${_buildDir}
   LOG_CONFIGURE 1 )
ExternalProject_Add_Step( Eigen buildinstall
   COMMAND ${CMAKE_COMMAND} -E copy_directory ${_buildDir}/ <INSTALL_DIR>
   COMMENT "Installing Eigen into the build area"
   DEPENDEES install )
add_dependencies( Package_Eigen Eigen )

# And now install it:
install( DIRECTORY ${_buildDir}/
   DESTINATION . USE_SOURCE_PERMISSIONS OPTIONAL )
