HDF5
====

This package builds [HDF5][1], a library to read and write generic
N-dimensional datasets. Bindings for C++ and C are supported by the
HDF group. For python bindings, see [h5py][3].

See the HDF group's [ftp site][2] for more versions.

[1]: https://en.wikipedia.org/wiki/Hierarchical_Data_Format
[2]: https://support.hdfgroup.org/ftp/HDF5/releases/
[3]: http://www.h5py.org/
